# readme.txt


# Branch strategy
masterブランチをメインに開発
自分の開発用ブランチを切って、実装完了次第masterにマージ

# git Address
https://suwabe0126k@bitbucket.org/suwabe0126k/timebomb.git/
パスワードを変更したいときは、上記リンクにアクセスしてアカウント情報を修正

# Command
クローン：git clone 
フェッチ:git fetch
プル:git pull
コミット:git commit <変更したファイル名1> <ファイル名2> -m "<コミットメッセージを記載>"
ブランチ確認：git branch
ブランチ変更：git checkout <ブランチ名を記載>
変更履歴を確認：git log


#Rule
Timebombのルールを記述