package com.example.koki.timebomb;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
//import android.content.Intent;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.widget.NumberPicker;
import android.media.MediaPlayer;
import java.io.IOException;
import java.util.Random;
import android.net.Uri;
import android.graphics.Color;

import android.content.res.Resources;
import android.widget.ImageView;
import android.widget.VideoView;

import android.content.Context;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;

public class MainActivity extends AppCompatActivity {
    //変数の定義
    int num_p = 2;//プレイヤー数
    int num_role_tmp = 2;
    int role_tmp[];
    int role[];
    int cnt = 0;
    int turn_count = 1;
    int cut_count = 0;
    int line[][];//導線カード
    int rest [] = new int[]{0,0};
    int rest2 [] = new int []{0,0};
    int result = 1;
    int tmp = 0;
    int max_ter[] = new int[]{0,2,2,2,2,2,3,3};
    int max_swa[] = new int[]{0,2,2,3,3,4,5,5};
    int max_spy[] = new int[]{0,1,1,1,1,1,1,1};
    int saf_cnt=0;
    int suc_cnt=0;
    int bom_cnt=0;
    TextView textView;
    TextView textView_test_text;
    EditText editText_player1,editText_player2,editText_player3,editText_player4,editText_player5,editText_player6,editText_player7,editText_player8;
    VideoView v;
    String Activity_name;
    NumberPicker numberPicker;
    NumberPicker picker;
    MediaPlayer mp;
    MediaPlayer v1;
    float vol = 1.0f;
    ImageView imageView;
    ImageView result_back_ground;
    String[] playerName = {"Player1", "Player2", "Player3","Player4","Player5", "Player6", "Player7","Player8"};
    String[] playerName_tmp;
    int nipper = 0 ;
    double d = 0;
    double e = 0;
    double f = 0;
    double g = 0;
    int result_cut = 0;
    int suc_cut = 0 ;
    int bom_cut = 0 ;
    int hand[] = new int[]{5,5,5,5,5,5,5,5};
    ImageButton imageButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.title);
        Activity_name = "title";
        if(Math.random()<0.5) {
            imageView = (ImageView) findViewById(R.id.imageView);
            imageView.setImageResource(R.drawable.title2);
        }
        num_p = 4;
        mp = MediaPlayer.create(this,R.raw.start_bgm);
        role = new int[num_p];
        line = new int[num_p][3];
        nipper = (int)( Math.random() * (double)num_p );
        for(int i = 0;i<num_p; i++){
            role[i] = 0;
            for(int j = 0;j<3; j++){
                line[i][j]=0;
            }
        }
        num_role_tmp = max_ter[num_p-1]+max_swa[num_p-1]+max_spy[num_p-1];
        role_tmp = new int[num_role_tmp];
        try {
            mp.setDataSource("raw/start_bgm.mp3");
        } catch (IllegalArgumentException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        } catch (IllegalStateException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        } catch (IOException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        }
        try {
            mp.prepare();
        } catch (IllegalStateException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        } catch (IOException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        }
        mp.start();
        mp.setLooping(true);

    }
    public void onClick_Start(View view){
        init();



        while(vol > 0.00f){
            vol = vol-0.05f;
            mp.setVolume(vol, vol);
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        if ( vol <= 0 ) {
            mp.release();
            mp = null;
        }
        setPlayer();
    }


    public void onClick_Settings(View view) {
//        mp.stop();
        setContentView(R.layout.set_player_num);
        Activity_name = "set_player_num";
        numberPicker = findViewById(R.id.numPicker);
        numberPicker.setMaxValue(8);
        numberPicker.setMinValue(2);
        numberPicker.setValue(4);
        player_name_set();
    }
    public void onClick_return(View view) {
//        setScreenMain();
    }
    public void onClick_setplayer(View view) {
        init();
        setPlayer();
    }


    private void player(){//各プレイヤーに役職を提示していくコード【友希】
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("あなたは"+playerName[cnt]+"ですか？")
                .setPositiveButton("はい", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {// ボタンをクリックしたときの動作
                        switch(role[cnt]){
                            case 0:
                                builder.setMessage("あなたの役職はSWATです。")
                                .setPositiveButton("はい", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
// ボタンをクリックしたときの動作
                                    if(cnt<num_p-1){
                                        cnt++;
                                        player();
                                    }
                                    else{
                                        cnt=0;
                                        textView.setText("導線カード確認フェイズ");
                                        show_line();
                                    }
                                }
                            });
                                break;
                            case 1:
                                builder.setMessage("あなたの役職はTerroristです。")
                                       .setPositiveButton("はい", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int id) {
// ボタンをクリックしたときの動作
                                    if(cnt<num_p-1){
                                        cnt++;
                                        player();
                                    }
                                    else{
                                        cnt=0;
                                        textView.setText("導線カード確認フェイズ");
                                        show_line();
                                    }
                                }
                            });
                                break;
                            case 2:
                                builder.setMessage("あなたの役職はspyです。")
                                        .setPositiveButton("はい", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
// ボタンをクリックしたときの動作
                                                if(cnt<num_p-1){
                                                    cnt++;
                                                    player();
                                                }
                                                else{
                                                    cnt=0;
                                                    textView.setText("導線カード確認フェイズ");
                                                    show_line();
                                                }
                                            }
                                        });
                                break;
                        }

                        builder.show();



                    }
                });
        builder.show();

    }

    private void show_line() {//各プレイヤーに導線カードを提示していくコード【友希】
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
        builder.setMessage("あなたは"+playerName[cnt]+ "ですか？")
                .setPositiveButton("はい", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {// ボタンをクリックしたときの動作
                                builder.setMessage("あなたは\nSafe:"+line[cnt][0]+"枚\nSuccess:"+line[cnt][1]+"枚\nBomb:"+line[cnt][2]+"枚\n持っています。")
                                        .setPositiveButton("はい", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int id) {
// ボタンをクリックしたときの動作
                                                if (cnt < num_p - 1) {
                                                    cnt++;
                                                    show_line();
                                                }
                                                else {
                                                    cnt = 0;
                                                    select_wire();
                                                }
                                            }
                                        });



                        builder.show();


                    }
                });
        builder.show();
    }
    private void select_wire() {
        textView = findViewById(R.id.test_text);
        textView.setText("導線切断フェイズ");
        textView = findViewById(R.id.nipper_text);
        playerName_tmp = new String[num_p-1];
        for(int i =0;i<num_p ; i++){
            if (i<nipper){
                playerName_tmp[i]= playerName[i];
            }
            else if(i>nipper){
                playerName_tmp[i-1]= playerName[i];
            }

        }

            picker = (NumberPicker) findViewById(R.id.selectwire);
            picker.setBackgroundColor(Color.WHITE);
            picker.setDisplayedValues(null);
            picker.setMaxValue(num_p - 2);
            picker.setMinValue(0);
            picker.setValue(0);
            picker.setDisplayedValues(playerName_tmp);


    }

    public void onClick_cut_button(View view) {
            Random rnd = new Random();
            if (picker.getValue() < nipper) {
                nipper = picker.getValue();
                d = Math.random();
                e = (double) (line[nipper][0]) / (hand[nipper]);
                f = (double) (line[nipper][1]) / (hand[nipper]);
                g = (double) (line[nipper][2]) / (hand[nipper]);
                if (d <= e) {
                    result_cut = 0;
                    cut_count++;
                    line[nipper][0] = line[nipper][0] - 1;
                    hand[nipper] = hand[nipper] - 1;
                    show_selected_line();
                } else if (e < d && d <= e + f) {
                    result_cut = 1;
                    suc_cut++;
                    cut_count++;
                    line[nipper][1] = line[nipper][1] - 1;
                    hand[nipper] = hand[nipper] - 1;
                    show_selected_line();
                } else if (e + f < d && d <= e + f + g) {
                    result_cut = 2;
                    cut_count++;
                    bom_cut++;
                    show_selected_line();
                }
            } else {
                nipper = picker.getValue() + 1;
                d = Math.random();
                e = (double) (line[nipper][0]) / (hand[nipper]);
                f = (double) (line[nipper][1]) / (hand[nipper]);
                g = (double) (line[nipper][2]) / (hand[nipper]);
                if (d < e) {
                    result_cut = 0;
                    cut_count++;
                    line[nipper][0] = line[nipper][0] - 1;
                    hand[nipper] = hand[nipper] - 1;
                    show_selected_line();
                } else if (e < d && d < e + f) {
                    result_cut = 1;
                    suc_cut++;
                    cut_count++;
                    line[nipper][1] = line[nipper][1] - 1;
                    hand[nipper] = hand[nipper] - 1;
                    show_selected_line();
                } else if (e + f < d && d < e + f + g) {
                    result_cut = 2;
                    cut_count++;
                    bom_cut++;
                    show_selected_line();
                }
            }
        }






    public void show_selected_line(){
        ImageButton imageButton =(ImageButton)findViewById(R.id.cut_button);
        imageButton.setEnabled(false);
        v = (VideoView) findViewById(R.id.v);
        v.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mediaPlayer) {
                judge();
            }
        });
        switch(result_cut) {
            case 0:
                v.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.safe));
                break;
            case 1:
                v.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.success));
                break;
            case 2:
                v.setVideoURI(Uri.parse("android.resource://" + getPackageName() + "/" + R.raw.bomb));
                break;
        }
        v.start();
//        v1.start();
    }
    public void judge(){
        if(suc_cut==num_p){
            result = 0;
            setContentView(R.layout.result);
            Activity_name = "result";
            show_result();

        }

        else if(bom_cut==1){
            result = 1;
            setContentView(R.layout.result);
            Activity_name = "result";
            show_result();

        }

        else if(cut_count==num_p*4){
            result = 2;
            setContentView(R.layout.result);
            Activity_name = "result";
            show_result();

        }

        else{
            textView = findViewById(R.id.nipper_text);
            textView.setText(playerName[nipper]+"のターンです\nターン"+turn_count+"\nsuccess:"+suc_cut+"\n");
            for(int i=0;i<num_p;i++){
                textView.setText(textView.getText()+playerName[i]+":"+String.valueOf(hand[i])+"\n");
            }
            ImageButton imageButton =(ImageButton)findViewById(R.id.cut_button);
            imageButton.setEnabled(true);
            turn_setting();
        }
    }

    public void turn_setting(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false);
                if(cut_count==(turn_count*num_p))
                    {
                        textView_test_text = findViewById(R.id.test_text);
                        textView_test_text.setText("ターン終了");
                        builder.setMessage("ターン"+turn_count+"終了")
                                .setPositiveButton("次のターン", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        turn_count++;
                                        reset_hand();
                                        textView = findViewById(R.id.nipper_text);
                                        textView.setText(playerName[nipper]+"のターンです\nターン"+turn_count+"\nsuccess:"+suc_cut+"\n");
                                        for(int i=0;i<num_p;i++){
                                            textView.setText(textView.getText()+"プレイヤー"+String.valueOf(i+1)+":"+String.valueOf(hand[i])+"\n");
                                        }
                                    }

                                });
                        builder.show();
                    }
                else

                    {
                        select_wire();
                    }
                }


    private void setPlayer(){
        for(int i = 0;i<num_p;i++){
            hand[i] = 5;
        }
        setContentView(R.layout.set_player);
        Activity_name = "set_player";
        mp = MediaPlayer.create(this,R.raw.discussion);
        vol = 1.00f;
        try {
            mp.setDataSource("raw/discussion.mp3");
        } catch (IllegalArgumentException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        } catch (IllegalStateException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        } catch (IOException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        }
        try {
            mp.prepare();
        } catch (IllegalStateException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        } catch (IOException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        }
        mp.start();
        mp.setLooping(true);
        textView = findViewById(R.id.test_text);
        textView.setText("役職確認フェイズ");
        player();
    }
    private void init(){//初期化を行う
        num_role_tmp = max_ter[num_p-1]+max_swa[num_p-1]+max_spy[num_p-1];
        role_tmp = new int[num_role_tmp];
        line = new int[num_p][3];//導線カード
        role = new int[num_p];
        nipper = (int)( Math.random() * (double)num_p );
        rest[0] = 4 * num_p-1;
        rest[1] = num_p;
        saf_cnt=0;
        suc_cnt=0;
        bom_cnt=0;
        turn_count = 1;
        cut_count = 0;
        suc_cut = 0;
        bom_cut = 0;
        for(int i = 0;i<num_p; i++){
            role[i] = 0;
            for(int j = 0;j<3; j++){
                line[i][j]=0;
            }
        }
        for(int i = 0;i < num_role_tmp; i++){
            role_tmp[i] =0;
            if(i >= num_role_tmp-max_ter[num_p-1]){
                role_tmp[i] = 1;
            }
            if(i==num_role_tmp-1){
                role_tmp[i] = 2;
            }

        }
        for ( int i = 0; i < num_role_tmp; i++ ) {
            int rnd = (int)( Math.random() * (double)num_role_tmp );// 0～(配列aryの個数-1)の乱数を発生
            int w = role_tmp[ i ];
            role_tmp[ i ] = role_tmp[ rnd ];
            role_tmp[ rnd ] = w;// role[ i ]とrole[ rnd ]を入れ替える
        }
        for( int i = 0;i<num_p; i++){
            role[i] = role_tmp[i];
        }
        distribute_line();

    }
    public void onClick_return_main(View view) {//Main画面に戻る
        mp.stop();
        setContentView(R.layout.title);
        Activity_name = "title";
        mp = MediaPlayer.create(this,R.raw.start_bgm);
        vol = 1.0f;
        try {
            mp.setDataSource("raw/start_bgm.mp3");
        } catch (IllegalArgumentException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        } catch (IllegalStateException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        } catch (IOException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        }
        try {
            mp.prepare();
        } catch (IllegalStateException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        } catch (IOException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        }
        mp.start();
        mp.setLooping(true);
//        init();

    }
    public void onClick_developer_mode(View view) {//内部値参照モードに移行
//        Activity_name = getLocalClassName();
//        Activity_name = getComponentName().getClassName();
        setContentView(R.layout.developer_mode);
        TextView textView = findViewById(R.id.variables_text);
        textView.setText("Current Activity:"+Activity_name+"\n");
        textView.setText(textView.getText()+"Player num:"+String.valueOf(num_p)+"\n");
        textView.setText(textView.getText()+"Role:");
        for(int i=0;i<num_p;i++){
            textView.setText(textView.getText()+String.valueOf(role[i])+"\t");
        }
        textView.setText(textView.getText()+"\n");
        for(int i=0;i<num_p;i++){
            textView.setText(textView.getText()+"Line(P"+String.valueOf(i)+"):");
            for(int j= 0;j<3;j++){
                textView.setText(textView.getText()+String.valueOf(line[i][j])+"\t");
            }
            textView.setText(textView.getText()+"\n");
        }
        textView.setText(textView.getText()+"Turn count:"+String.valueOf(turn_count)+"\n");
        textView.setText(textView.getText()+"Cut count:"+String.valueOf(cut_count)+"\n");
        for(int i=0;i<2;i++){
            textView.setText(textView.getText()+String.valueOf(rest[i])+"\t");
        }
        textView.setText(textView.getText()+"\n");
        textView.setText(textView.getText()+String.valueOf(tmp)+"\n");
        for(int i=0;i<num_p;i++){
            textView.setText(textView.getText()+playerName[i]+"\t");
        }
        textView.setText(textView.getText()+"\n");
        for(int i=0;i<num_p-1;i++){
            textView.setText(textView.getText()+playerName_tmp[i]+"\t");
        }
        textView.setText(textView.getText()+"\n"+String.valueOf(d));
        textView.setText(textView.getText()+"\n"+String.valueOf(e));
        textView.setText(textView.getText()+"\n"+String.valueOf(f));
        textView.setText(textView.getText()+"\n"+String.valueOf(g));
        textView.setText(textView.getText()+"\n"+String.valueOf(result_cut));
        textView.setText(textView.getText()+"\n"+String.valueOf(hand[0]));
        textView.setText(textView.getText()+"\n"+String.valueOf(hand[1]));
        textView.setText(textView.getText()+"\n"+String.valueOf(hand[2]));
        textView.setText(textView.getText()+"\n"+String.valueOf(hand[3]));
        textView.setText(textView.getText()+"\n"+String.valueOf(turn_count));
        int id = getResources().getIdentifier(Activity_name, "layout", getPackageName());
        textView.setText(textView.getText()+"\nid:"+String.valueOf(id));
    }
    public void onClick_init(View view) {//Developer modeで初期化し直す
        setContentView(R.layout.title);
        Activity_name = "title";
        if(Math.random()<0.5) {
            imageView = (ImageView) findViewById(R.id.imageView);
            imageView.setImageResource(R.drawable.title2);
        }
//        num_p = 4;


        for(int i = 0;i<num_p; i++){
            role[i] = 0;
            for(int j = 0;j<3; j++){
                line[i][j]=0;
            }
        }
        num_role_tmp = max_ter[num_p-1]+max_swa[num_p-1]+max_spy[num_p-1];
        role_tmp = new int[num_role_tmp];
        mp = MediaPlayer.create(this,R.raw.start_bgm);
        vol = 1.00f;
        try {
            mp.setDataSource("raw/start_bgm.mp3");
        } catch (IllegalArgumentException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        } catch (IllegalStateException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        } catch (IOException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        }
        try {
            mp.prepare();
        } catch (IllegalStateException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        } catch (IOException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        }
        mp.start();
        mp.setLooping(true);

    }
    public void distribute_line(){
        int total = rest[0]+rest[1]+1;
        int line_tmp [] =new int[total];
        for(int i = 0;i < total; i++){
            if(i<rest[1]){
                line_tmp[i] = 1;
            }
            else {
                line_tmp[i] = 0;
            }
        }
        line_tmp[total-1] = 2;
        for ( int i = 0; i < total; i++ ) {
            int rnd = (int)( Math.random() * (double)total );// 0～(配列aryの個数-1)の乱数を発生
            int w = line_tmp[ i ];
            line_tmp[ i ] = line_tmp[ rnd ];
            line_tmp[ rnd ] = w;// role[ i ]とrole[ rnd ]を入れ替える
        }

        for(int i =0;i<num_p;i++){
            saf_cnt=0;
            suc_cnt=0;
            bom_cnt=0;
            for(int j =5*i;j<5*(i+1);j++){
                switch(line_tmp[j]){
                    case 0:
                        saf_cnt++;
                        break;
                    case 1:
                        suc_cnt++;
                        break;
                    case 2:
                        bom_cnt++;
                        break;
                }
                line[i][0]=saf_cnt;
                line[i][1]=suc_cnt;
                line[i][2]=bom_cnt;
            }
        }
    }

    public void reset_hand() {
        for(int i =0;i<num_p;i++){
            hand[i]=6-turn_count;
        }
        rest2[0]= 0;
        rest2[1]= 0;
        for(int j = 0;j<num_p;j++){
            rest2[0]=rest2[0]+line[j][0];
            rest2[1]=rest2[1]+line[j][1];
        }
        int total = rest2[0] + rest2[1] + 1;
        int line_tmp[] = new int[total];
        for (int i = 0; i < total; i++) {
            if (i < rest2[1]) {
                line_tmp[i] = 1;
            } else {
                line_tmp[i] = 0;
            }
        }
        line_tmp[total - 1] = 2;
        for (int i = 0; i < total; i++) {
            int rnd = (int) (Math.random() * (double) total);// 0～(配列aryの個数-1)の乱数を発生
            int w = line_tmp[i];
            line_tmp[i] = line_tmp[rnd];
            line_tmp[rnd] = w;// role[ i ]とrole[ rnd ]を入れ替える
        }
        for(int i =0;i<num_p;i++){
            int saf_cnt=0;
            int suc_cnt=0;
            int bom_cnt=0;
            for(int j =(6-turn_count)*i;j<(6-turn_count)*(i+1);j++){
                switch(line_tmp[j]){
                    case 0:
                        saf_cnt++;
                        break;
                    case 1:
                        suc_cnt++;
                        break;
                    case 2:
                        bom_cnt++;
                        break;
                }
                line[i][0]=saf_cnt;
                line[i][1]=suc_cnt;
                line[i][2]=bom_cnt;
            }
        }

        show_line();
    }
    public void onClick_result(View view) {
        setContentView(R.layout.result);
        Activity_name = "result";
        show_result();

    }
    public void show_result(){
        mp.stop();
        TextView resultView = findViewById(R.id.result);
        TextView resultView2 = findViewById(R.id.result_text0);
        result_back_ground = findViewById(R.id.result_back_ground);
        if(result ==0){
//            resultView.setText("SWATの勝利！");
            mp = MediaPlayer.create(this,R.raw.swat_win);
            result_back_ground.setImageResource(R.drawable.swat_win_image);
        }
        else if(result ==1){
//            resultView.setText("Terroristの勝利！");
            mp = MediaPlayer.create(this,R.raw.terrorist_win);
            result_back_ground.setImageResource(R.drawable.terrorist_win_image);
        }
        else if(result ==2){
            mp = MediaPlayer.create(this,R.raw.spy_win);
            result_back_ground.setImageResource(R.drawable.spy_win_image);
        }
        for(int i =0;i<num_p;i++){
            if(role[i]==0){
                resultView2.setText(resultView2.getText()+playerName[i]+":SWAT"+"\n");
            }
            else if(role[i]==1){
                resultView2.setText(resultView2.getText()+playerName[i]+":Terrorist"+"\n");
            }
            else{
                resultView2.setText(resultView2.getText()+playerName[i]+":Spy"+"\n");
            }
        }
        cut_count=0;
        suc_cut=0;
        bom_cut = 0;
        turn_count=1;

        vol = 1.00f;
        try {
            mp.setDataSource("raw/start_bgm.mp3");
        } catch (IllegalArgumentException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        } catch (IllegalStateException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        } catch (IOException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        }
        try {
            mp.prepare();
        } catch (IllegalStateException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        } catch (IOException e) {
            // TODO 自動生成された catch ブロック
            e.printStackTrace();
        }
        mp.start();
        mp.setLooping(true);
    }
    public void onClick_set_player_to_main(View view) {
        num_p = numberPicker.getValue();
        nipper = (int)( Math.random() * (double)num_p );
        num_role_tmp = max_ter[num_p-1]+max_swa[num_p-1]+max_spy[num_p-1];
        role_tmp = new int[num_role_tmp];
        line = new int[num_p][3];//導線カード
        role = new int[num_p];
//        editText_player1 = findViewById(R.id.edit_text1);
//        editText_player2 = findViewById(R.id.edit_text2);
//        editText_player3 = findViewById(R.id.edit_text3);
//        editText_player4 = findViewById(R.id.edit_text4);
//        editText_player5 = findViewById(R.id.edit_text5);
//        editText_player6 = findViewById(R.id.edit_text6);
//        editText_player7 = findViewById(R.id.edit_text7);
//        editText_player8 = findViewById(R.id.edit_text8);
//        playerName[0] = editText_player1.getText().toString();
//        playerName[1] = editText_player2.getText().toString();
//        playerName[2] = editText_player3.getText().toString();
//        playerName[3] = editText_player4.getText().toString();
//        playerName[4] = editText_player5.getText().toString();
//        playerName[5] = editText_player6.getText().toString();
//        playerName[6] = editText_player7.getText().toString();
//        playerName[7] = editText_player8.getText().toString();
//        init();
        setContentView(R.layout.title);
        Activity_name = "title";
    }

    public void onClick_back(View view) {
        int id = getResources().getIdentifier(Activity_name, "layout", getPackageName());
        setContentView(id);
    }

    public EditText editText;
    public String fileName = "testfile.txt";


    public void player_name_set(){
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);


//        FileInputStream fileInputStream = openFileInput("testfile.txt");
//        textView = findViewById(R.id.text_view);

//        editText = findViewById(R.id.edit_text1);
        editText_player1 = findViewById(R.id.edit_text1);
        editText_player2 = findViewById(R.id.edit_text2);
        editText_player3 = findViewById(R.id.edit_text3);
        editText_player4 = findViewById(R.id.edit_text4);
        editText_player5 = findViewById(R.id.edit_text5);
        editText_player6 = findViewById(R.id.edit_text6);
        editText_player7 = findViewById(R.id.edit_text7);
        editText_player8 = findViewById(R.id.edit_text8);


        Button buttonSave = findViewById(R.id.button_save);
        buttonSave.setOnClickListener(new View.OnClickListener() {
//            @Override
            public void onClick(View v) {
                // エディットテキストのテキストを取得
                    String text1 = readFile(fileName) + "\n" +editText_player1.getText().toString();
                     saveFile(fileName, text1);
                    playerName[0] = readFile(fileName);
                    String text2 = readFile(fileName) + "\n" + editText_player2.getText().toString();
                    saveFile(fileName, text2);
                    playerName[1] = readFile(fileName);
                    String text3 = readFile(fileName) + "\n" + editText_player3.getText().toString();
                    saveFile(fileName, text3);
                    playerName[2] = readFile(fileName);
                    String text4 = readFile(fileName) + "\n" + editText_player4.getText().toString();
                    saveFile(fileName, text4);
                    playerName[3] = readFile(fileName);
                    String text5 = readFile(fileName) + "\n" + editText_player5.getText().toString();
                    saveFile(fileName, text5);
                    playerName[4] = readFile(fileName);
                    String text6 = readFile(fileName) + "\n" + editText_player6.getText().toString();
                    saveFile(fileName, text6);
                    playerName[5] = readFile(fileName);
                    String text7 = readFile(fileName) + "\n" + editText_player7.getText().toString();
                    saveFile(fileName, text7);
                    playerName[6] = readFile(fileName);
                    String text8 = readFile(fileName) + "\n" + editText_player8.getText().toString();
                    saveFile(fileName, text8);
                    playerName[7] = readFile(fileName);




//                        if (text1.length() == 0) {
//                            textView.setText(R.string.no_text);
//                        } else {
//                            textView.setText(R.string.saved);
//                        }




                }
        });

//        Button buttonRead = findViewById(R.id.button_read);
//        buttonRead.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String str = readFile(fileName);
//                if (str != null) {
//                    textView.setText(str);
//                } else {
//                    textView.setText(R.string.read_error);
//                }
//            }
//        });

    }

    // ファイルを保存
    public void saveFile(String file, String str) {

        // try-with-resources
        try (FileOutputStream  fileOutputstream = openFileOutput(file,
                Context.MODE_PRIVATE);){

            fileOutputstream.write(str.getBytes());

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    // ファイルを読み出し
    public String readFile(String file) {
        String text = null;

        // try-with-resources
        try (FileInputStream fileInputStream = openFileInput(file);
             BufferedReader reader= new BufferedReader(
                     new InputStreamReader(fileInputStream,"UTF-8"));
        ) {

            String lineBuffer;
            while( (lineBuffer = reader.readLine()) != null ) {
                text = lineBuffer ;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        return text;
    }

}


